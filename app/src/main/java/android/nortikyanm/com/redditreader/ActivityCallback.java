package android.nortikyanm.com.redditreader;

import android.net.Uri;


public interface ActivityCallback {
    void onPostSelected(Uri redditPostUri);
}